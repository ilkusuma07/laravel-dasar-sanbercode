<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("Shaun");
    echo  "Name : " . $sheep->name . "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br>"; // "no"
    

    $kodok = new Frog("buduk");
    echo  "<br> Name : " . $kodok->name . "<br>"; 
    echo "Legs : " . $kodok->legs . "<br>"; 
    echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; 
    echo "Jump : " . $kodok->jump . "<br>"; 

    $ape = new Ape("Kera Sakti");
    echo  "<br> Name : " . $ape->name . "<br>"; 
    echo "Legs : " . $ape->legs . "<br>"; 
    echo "Cold Blooded : " . $ape->cold_blooded . "<br>"; 
    echo "Yell : " . $ape->yell . "<br>"; 
?>