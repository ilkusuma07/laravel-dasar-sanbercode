<?php 

class Animal{
    public $name;
    public $legs =4;
    public $cold_blooded = "no";

    public function __construct($nama)
    {
        $this->name =$nama;
    }
}
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>