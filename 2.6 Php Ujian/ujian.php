<?php
    function ubah_huruf($string){
        $abjad= "abcdefghijklmnopqrstuvwxyz";
        $karakterAbjad = str_split($abjad);
        $karakterUbah = str_split($string);
        foreach ($karakterUbah as $huruf){
            $strpos = strpos($abjad, $huruf);
            $tampung = $strpos +1;
            echo $karakterAbjad[$tampung];
        }
    }
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu
?>